<?php

namespace RKT\Domains\Events;


use RKT\Domains\Repositories\EventLogRepository;

class ChangeDBEvent extends BaseEvent
{
    public function __construct($id, $model, $event_type = null)
    {
        parent::__construct(EventLogRepository::class, $id, $model, $event_type);
    }
}