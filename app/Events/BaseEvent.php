<?php

namespace RKT\Domains\Events;



use Illuminate\Queue\SerializesModels;


abstract class BaseEvent
{
    use SerializesModels;

    protected $eventLogRepository;
    protected $changed_model;
    protected $changed_id;
    protected $event_type;

    public function __construct($eventLogRepository = null, $changed_id = null, $changed_model = null, $event_type = null)
    {
        $this->eventLogRepository = $eventLogRepository;
        $this->changed_id = $changed_id;
        $this->changed_model = $changed_model;
        $this->event_type = $event_type;
    }

    public function getEventLogRepository(){
        return $this->eventLogRepository;
    }

    public function getChangedModel(){
        return $this->changed_model;
    }

    public function getChangedId(){
        return $this->changed_id;
    }

    public function getEventType(){
        return $this->event_type;
    }

}