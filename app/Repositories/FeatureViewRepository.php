<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use RKT\Domains\Models\FeatureView;

class FeatureViewRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(FeatureView::class);
    }

    public function reportModel($select, $between, $groupBy, $conditions, $loads = []){
        return $this->model->with($loads)
            ->join('client_features', 'client_features.id', '=', 'feature_views.client_feature_id')
            ->join('features', 'client_features.feature_id', '=', 'features.id')
            ->where($conditions)
            ->whereBetween('feature_views.created_at', $between)
            ->groupBy($groupBy)
            ->selectRaw('count(feature_views.id) as impressions,'.$select)
            ->get();
    }
}