<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Customer;

class CustomerRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Customer::class);
    }
}