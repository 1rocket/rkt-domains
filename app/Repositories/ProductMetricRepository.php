<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use RKT\Domains\Models\ProductMetric;

class ProductMetricRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(ProductMetric::class);
    }

    public function mostViewedByClient($client_id, $days, $limit=null, $department=null, $brand=null){
        $query = $this->model->with(['product'])
            ->selectRaw('sum(views) as views, product_id, p_id, sku, department, brand')
            ->where('client_id', $client_id)
            ->where('created_at', '>', Carbon::today()->subDays($days))
            ->orderBy('views', 'desc');

        if($limit != null)
            $query->limit($limit);

        if($department != null)
            $query->where('department', $department);

        if($brand != null)
            $query->where('brand', $brand);

        return $query->get();
    }

    public function mostAddToCartByClient($client_id, $days, $limit=null, $department=null, $brand=null){
        $query = $this->model->with(['product'])
            ->selectRaw('sum(carts) as carts, product_id, p_id, sku, department, brand')
            ->where('client_id', $client_id)
            ->where('created_at', '>', Carbon::today()->subDays($days))
            ->orderBy('carts', 'desc');

        if($limit != null)
            $query->limit($limit);

        if($department != null)
            $query->where('department', $department);

        if($brand != null)
            $query->where('brand', $brand);

        return $query->get();
    }

    public function mostPurchasesByClient($client_id, $days, $limit=null, $department=null, $brand=null){
        $query = $this->model->with(['product'])
            ->selectRaw('sum(purchases) as purchases, product_id, p_id, sku, department, brand')
            ->where('client_id', $client_id)
            ->where('created_at', '>', Carbon::today()->subDays($days))
            ->orderBy('purchases', 'desc');

        if($limit != null)
            $query->limit($limit);

        if($department != null)
            $query->where('department', $department);

        if($brand != null)
            $query->where('brand', $brand);

        return $query->get();
    }
}
