<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\ClientPlan;

class ClientPlanRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(ClientPlan::class);
    }
}
