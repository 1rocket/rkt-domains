<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use RKT\Domains\Enums\PackageAppType;
use RKT\Domains\Models\ClientFeature;

class ClientFeatureRepository extends BaseRepository {

    public function __construct(){
        parent::__construct(ClientFeature::class);
    }

    public function findAvailableFeaturesBy($conditions){
        return $this->model->with('feature_theme')
            ->join('features', 'client_features.feature_id', '=', 'features.id')
            ->join('apps', 'features.app_id',  '=', 'apps.id')
            ->join('package_apps', 'package_apps.app_id', '=', 'apps.id')
            ->join('client_package_apps', 'client_package_apps.package_app_id', '=', 'package_apps.id')
            ->where($conditions)
            ->whereRaw('((now() > client_package_apps.expire - interval 1 month AND client_package_apps.expire > now()) OR client_package_apps.type = ' . PackageAppType::BOUGHT . ')')
            ->whereRaw('(client_package_apps.consumed < package_apps.quantity || client_package_apps.unlimited = true)')
            ->groupBy('client_features.id')
            ->orderBy('apps.id', 'asc')
            ->select(
                'client_features.id',
                'client_features.feature_id',
                'client_features.config',
                'client_features.feature_theme_id',
                'features.name',
                'features.app_id')
            ->get();
    }
}
