<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\ClientPreset;

class ClientPresetRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(ClientPreset::class);
    }

    public function getLastPreset($conditions){
        return $this->model
            ->where($conditions)
            ->orderBy('created_at', 'DESC')
            ->first();
    }
}