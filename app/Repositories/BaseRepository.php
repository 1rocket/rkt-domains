<?php


namespace RKT\Domains\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use RKT\Domains\Events\ChangeDBEvent;
use RKT\Domains\Traits\HashId;
use RKT\Domains\Traits\UsesTrait;

abstract class BaseRepository
{
    use UsesTrait;

    protected $model;
    protected $modelName;
    protected $tableName;
    protected $queryBuilder;

    /**
     * BaseRepository constructor.
     * @param Model $model
     * @throws \ReflectionException
     */
    public function __construct($model = null)
    {
        if ($model)
            $this->setModelInstance($model);

    }

    //protected function beforeCreate(){
    //    $this->model->token = hash('sha256', microtime());;
    //}

    public function getTableName()
    {
        return $this->tableName;
    }

    /***
     * @return Builder
     */
    public function query(): Builder
    {
        return \DB::table($this->tableName);
    }

    /**
     * sets the base model
     * @param $model
     * @return $this
     * @throws \ReflectionException
     */
    public function model($model)
    {
        $this->setModelInstance($model);

        return $this;
    }

    public function getModelName(){
        return $this->modelName;
    }

    /***
     * @param $id
     * @return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    /***
     * @param $conditions
     * @return boolean
     */
    public function deleteWhere($conditions)
    {
        return $this->model->where($conditions)->delete();
    }

    /***
     * @param $id
     * @return mixed
     */
    public function restore($id)
    {
        return $this->model->find($id)->restore();
    }

    /**
     * @param $id
     * @return mixed
     */

    public function findBy($conditions, $loads = []){
        return $this->model->with($loads)
            ->where($conditions)
            ->get();
    }

    public function findByDate($column, $initial, $final, $loads = []){
        return $this->model->with($loads)
            ->whereBetween($column, [$initial, $final])
            ->get();
    }

    public function all($loads = [])
    {
        return $this->model->with($loads)->get();
    }


    /***
     * @param array $data
     * @return mixed $record
     * @throws \Exception
     */
    public function save(array $data, array $visible = null,$conditions = null)
    {
        $record = null;
        $this->model->makeVisible($visible)->fill($data);
        $keyName = $this->model->getKeyName();

        try {
            if ((isset($data[$keyName]) && $data[$keyName]) || $conditions) {
                if($conditions) {
                    //dd($conditions);
                    $record = $this->model->updateOrCreate($conditions, $this->model->toArray());
                }else {
                    $key = $data[$keyName];
                    $this->model->{$keyName} = $key;

                    $record = $this->model->updateOrCreate([$keyName => $key], $this->model->toArray());
                }
            } else {
                if (method_exists($this, 'beforeCreate'))
                    $this->beforeCreate();
                if ($this->usesTrait($this->modelName, HashId::class))
                    $this->model->{$keyName} = $this->model->generateHash();

                $record = $this->model->create($this->model->toArray());
            }

            return $record;
        }
        catch (\Exception $ex) {
            throw $ex;
        }

    }

    /***
     * @param string $model
     * @throws \ReflectionException
     */
    private function setModelInstance($model)
    {
        $this->modelName = $model;
        $this->model     = (new \ReflectionClass($model))->newInstance();
        if (!($this->model instanceof Model))
            dd("\$model is not a valid Model");

        $this->tableName    = $this->model->getTable();
        $this->queryBuilder = $this->query();
    }
}
