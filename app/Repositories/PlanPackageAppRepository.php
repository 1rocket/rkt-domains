<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\PlanPackageApp;

class PlanPackageAppRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(PlanPackageApp::class);
    }

    public function findPlanPackageAppsByApp($conditions){
        return $this->model
            ->select('apps.id AS app_id', 'apps.name AS app_name', 'apps.description', 'apps.quick_text', 'apps.logo', 'apps.images', 'apps.videos', 'plans.id AS plan_id', 'plans.name AS plan_name', 'plans.type AS plan_type', 'plans.real', 'plans.euro', 'plans.dollar', 'package_apps.quantity', 'plans.status')
            ->join('package_apps', 'package_apps.id', '=', 'plan_package_apps.package_app_id')
            ->join('plans', 'plan_package_apps.plan_id', '=', 'plans.id')
            ->join('apps', 'package_apps.app_id', '=', 'apps.id')
            ->where($conditions)
            ->get();
    }
}
