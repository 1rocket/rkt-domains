<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use RKT\Domains\Models\ClientPromotion;

class ClientPromotionRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(ClientPromotion::class);
    }
}
