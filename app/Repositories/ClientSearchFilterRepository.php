<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\ClientSearchFilter;

class ClientSearchFilterRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(ClientSearchFilter::class);
    }
}