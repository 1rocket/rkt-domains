<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Plan;

class PlanRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Plan::class);
    }
}
