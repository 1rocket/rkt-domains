<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\CustomerData;

class CustomerDataRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(CustomerData::class);
    }
}