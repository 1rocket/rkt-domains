<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Client;

class ClientRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Client::class);
    }
}
