<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\CategoryView;

class CategoryViewRepository extends BaseRepository{
    public function __construct(){
        parent::__construct(CategoryView::class);
    }

    public function findMostAccess($client_id, $limit){
        return $this->model
            ->join('sessions', 'sessions.id', '=', 'category_views.session_id')
            ->where('sessions.client_id', $client_id)
            ->select('category_views.category', 'url')
            ->groupBy('category_views.category')
            ->limit($limit)
            ->get();
    }
}