<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Intelligence;

class IntelligenceRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Intelligence::class);
    }
}
