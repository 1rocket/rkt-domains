<?php


namespace RKT\Domains\Repositories;


use Illuminate\Support\Facades\DB;
use RKT\Domains\Models\FeatureReport;

class FeatureReportRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(FeatureReport::class);
    }

    public function listReportsByFeature($feature_id, $between, $groupBy, $dateFormat){
        if($dateFormat == 'hour')
            $dateFormat = '%Y-%m-%d %H:00:00';
        else
            $dateFormat = '%Y-%m-%d';

        return $this->model
            ->select(['client_feature_id',
                DB::raw('SUM(feature_reports.impressions) as impressions'),
                DB::raw('DATE_FORMAT(feature_reports.created_at, "'. $dateFormat .'") as created_at')
            ])
            ->where('client_feature_id', $feature_id)
            ->whereBetween('created_at', $between)
            ->groupBy($groupBy)
            ->get();
    }

}