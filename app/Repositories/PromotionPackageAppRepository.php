<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\PromotionPackageApp;

class PromotionPackageAppRepository extends BaseRepository
{
    public function __construct(){
        parent::__construct(PromotionPackageApp::class);
    }
}
