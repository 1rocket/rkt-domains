<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Role;

class RoleRepository extends BaseRepository
{
    public function __construct(){
        parent::__construct(Role::class);
    }
}
