<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\SortingTerm;

class SortingTermRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(SortingTerm::class);
    }

}