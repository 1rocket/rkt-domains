<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use RKT\Domains\Models\CartProduct;

class CartProductRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(CartProduct::class);
    }

    public function getProductCarts($conditions, $between){
        return $this->model
            ->selectRaw('count(id) as carts, product_id')
            ->where($conditions)
            ->whereBetween('created_at', $between)
            ->groupBy('product_id')
            ->get();
    }

    public function findPeopleNow($client_id, $days, $product_id){
        $query = $this->model
            ->where('client_id', $client_id)
            ->where('created_at', '>', Carbon::today()->subDays($days))
            ->groupBy('cart_id');

        if($product_id)
            $query = $query->where('product_id', $product_id);

        return $query->get();

    }

    public function getNumCartsByClientId($client_id, $days, $getRecords) {
        $query = $this->model->where('client_id', $client_id)
            ->where('created_at', '>=', Carbon::now()->addDays(-$days))
            ->selectRaw('count(id) as carts, product_id')
            ->groupBy('p_id');

        if($getRecords){
            return $query->get();
        } else {
            return vsprintf(str_replace(array('?'), array('\'%s\''), $query->toSql()), $query->getBindings());
        }
    }
}
