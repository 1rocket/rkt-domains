<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Feature;

class FeatureRepository extends BaseRepository {

    public function __construct(){
        parent::__construct(Feature::class);
    }
}