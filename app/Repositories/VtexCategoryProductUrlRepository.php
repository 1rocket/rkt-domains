<?php


namespace RKT\Domains\Repositories;

use RKT\Domains\Models\VtexCategoryProductUrl;

class VtexCategoryProductUrlRepository extends BaseRepository
{
    public function __construct(VtexCategoryProductUrl $model)
    {
        parent::__construct($model);
    }

}
