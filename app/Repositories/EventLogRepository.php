<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\EventLog;

class EventLogRepository extends BaseRepository
{
    public function __construct(){
        parent::__construct(EventLog::class);
    }
}