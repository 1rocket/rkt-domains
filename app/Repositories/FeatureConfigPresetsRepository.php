<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\FeatureConfigPreset;

class FeatureConfigPresetsRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(FeatureConfigPreset::class);
    }

}