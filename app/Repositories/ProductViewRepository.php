<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use RKT\Domains\Models\ProductView;

class ProductViewRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(ProductView::class);
    }

    public function getNumViews($product_id, $days) {
        return $this->model->whereIn('product_id', $product_id)
            ->where('created_at', '>=', Carbon::now()->addDays(-$days))
            ->selectRaw('count(id) as views, product_id')
            ->groupBy('p_id')
            ->get();
    }

    public function getProductMetrics($conditions, $between){

        $productViews =
            $this->model
                ->selectRaw('count(id) as views, null as purchases, null as carts, null as searches, product_id')
                ->where($conditions)
                ->whereNotNull('product_id')
                ->whereBetween('created_at', $between)
                ->orderBy('product_id', 'ASC')
                ->groupBy('product_id');

        $purchaseProducts =
            DB::table('purchase_products')
                ->selectRaw('null as views, count(id) as purchases, null as carts, null as searches, product_id')
                ->where($conditions)
                ->whereNotNull('product_id')
                ->whereBetween('created_at', $between)
                ->orderBy('product_id', 'ASC')
                ->groupBy('product_id');

        $cartProducts =
            DB::table('cart_products')
                ->selectRaw('null as views, null as purchases, count(id) as carts, null as searches, product_id')
                ->where($conditions)
                ->whereNotNull('product_id')
                ->whereBetween('created_at', $between)
                ->orderBy('product_id', 'ASC')
                ->groupBy('product_id');

        $searchProducts =
            DB::table('search_terms')
                ->selectRaw('null as views, null as purchases, null as carts, count(id) as searches, product_id')
                ->where($conditions)
                ->whereNotNull('product_id')
                ->whereBetween('created_at', $between)
                ->orderBy('product_id', 'ASC')
                ->groupBy('product_id');

        $unionAll =
            $productViews
                ->unionAll($purchaseProducts)
                ->unionAll($cartProducts)
                ->unionAll($searchProducts);

        return DB::table($unionAll, 'unions')
            ->selectRaw('MD5(CONCAT(NOW(),unions.product_id)) as id, products.price, products.department, products.p_id, products.sku, products.brand, products.client_id, SUM(unions.views) as views, SUM(unions.purchases) as purchases, SUM(unions.carts) as  carts, SUM(unions.searches) as searches, unions.product_id')
            ->leftJoin('products', 'products.id', '=', 'unions.product_id')
            ->groupBy('unions.product_id')
            ->get();
    }

    public function getNumViewsByClientId($client_id, $days, $getRecords) {
        $query = $this->model->where('client_id', $client_id)
            ->where('created_at', '>=', Carbon::now()->addDays(-$days))
            ->selectRaw('count(id) as views, product_id')
            ->groupBy('p_id');

        if($getRecords){
            return $query->get();
        } else {
            return vsprintf(str_replace(array('?'), array('\'%s\''), $query->toSql()), $query->getBindings());
        }
    }
}
