<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use RKT\Domains\Models\ClientPackageApp;

class ClientPackageAppRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(ClientPackageApp::class);
    }

    public function findByApp($client_id, $app_id)
    {
        return $this->model
            ->join('package_apps', 'package_apps.id', '=', $this->getTableName() .'.'. 'package_app_id')
            ->where(['client_id' => $client_id, 'package_apps.app_id' => $app_id])
            ->first();
    }

    public function updateConsumption($client_id, $app_id, $count){
        $record = $this->model->select(['client_package_apps.id', 'client_package_apps.consumed'])
            ->join('package_apps', 'package_apps.id', '=', 'client_package_apps.package_app_id')
            ->where('client_package_apps.client_id', $client_id)
            ->where('client_package_apps.expire', '>', Carbon::now())
            ->where('package_apps.app_id', $app_id)
            ->whereRaw('client_package_apps.consumed < package_apps.quantity')
            ->orderBy('client_package_apps.type', 'asc')
            ->orderBy('client_package_apps.expire', 'asc')
            ->limit(1)
            ->first();


        if($record)
            $record = $this->model->where(['id' => $record->id])
                ->update(['consumed' => $record->consumed + $count]);

        return $record;

    }

    public function updatePeriod($days){
        return $this->model
            ->where('period', '>', 0)
            ->increment('period', $days);
    }
}
