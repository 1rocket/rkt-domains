<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\LogPayg;

class LogPaygRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(LogPayg::class);
    }
}
