<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\PageView;

class PageViewRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(PageView::class);
    }
}