<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Platform;

class PlatformRepository extends BaseRepository
{
    public function __construct(){
        parent::__construct(Platform::class);
    }
}
