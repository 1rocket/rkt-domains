<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\PurchaseProduct;

class PurchaseProductRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(PurchaseProduct::class);
    }

    public function findLast($product_id, $loads = []){
        return $this->model->with($loads)
            ->where('product_id', $product_id)
            ->orderBy('created_at', 'DESC')
            ->first();
    }
    public function getProductPurchases($conditions, $between){
        return $this->model
            ->selectRaw('count(id) as purchases, product_id')
            ->where($conditions)
            ->whereBetween('created_at', $between)
            ->groupBy('product_id')
            ->get();
    }


    public function getNumPurchasesByClientId($client_id, $days, $getRecords) {
        $query = $this->model->where('client_id', $client_id)
            ->where('created_at', '>=', Carbon::now()->addDays(-$days))
            ->selectRaw('count(id) as purchases, product_id')
            ->groupBy('p_id');

        if($getRecords){
            return $query->get();
        } else {
            return vsprintf(str_replace(array('?'), array('\'%s\''), $query->toSql()), $query->getBindings());
        }
    }
}
