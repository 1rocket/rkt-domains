<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Event;

class EventRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Event::class);
    }
}
