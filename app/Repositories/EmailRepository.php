<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Email;

class EmailRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Email::class);
    }

    public function findOrCreate($data){
        $record = null;
        try {
                $record = $this->model->firstOrCreate(['email' => $data['email']]);

                return $record;

        }catch (\Exception $ex){
            throw $ex;
        }
    }
}