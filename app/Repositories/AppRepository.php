<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\App;

class AppRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(App::class);
    }

    public function findFeaturesByAppId($conditions){
        return $this->model
        ->select('client_features.*')
            ->join('features', 'apps.id', '=', 'features.app_id')
            ->join('client_features', 'features.id', '=', 'client_features.feature_id')
            ->where($conditions)
            ->get();
    }
}
