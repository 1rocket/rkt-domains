<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\PackageApp;

class PackageAppRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(PackageApp::class);
    }
}
