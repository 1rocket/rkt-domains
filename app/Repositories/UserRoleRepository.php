<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\UserRole;

class UserRoleRepository extends BaseRepository
{

    public function __construct(){
        parent::__construct(UserRole::class);
    }
}
