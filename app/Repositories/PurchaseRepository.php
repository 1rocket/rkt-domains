<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use RKT\Domains\Models\Purchase;

class PurchaseRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Purchase::class);
    }

    public function findByDays($client_id, $days, $discount){
        $query = $this->model
            ->where('client_id', $client_id)
            ->where('created_at', '>', Carbon::today()->subDays($days));

        if($discount)
            $query = $query->where('discount', '>', 0);

        return $query->get();
    }
}