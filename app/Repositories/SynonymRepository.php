<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Synonym;

class SynonymRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Synonym::class);
    }

}