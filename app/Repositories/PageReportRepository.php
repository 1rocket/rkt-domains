<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\PageReport;

class PageReportRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(PageReport::class);
    }
}