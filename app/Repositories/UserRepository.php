<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\User;

class UserRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(User::class);
    }

    public function byEmail($email){
        return User::where('email', trim($email))->with(['user_roles'])->first();
    }

}
