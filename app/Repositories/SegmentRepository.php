<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Segment;

class SegmentRepository extends BaseRepository
{
    public function __construct(){
        parent::__construct(Segment::class);
    }
}
