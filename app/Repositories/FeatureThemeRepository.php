<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\FeatureTheme;

class FeatureThemeRepository extends BaseRepository
{
    public function __construct(FeatureTheme $model)
    {
        parent::__construct($model);
    }

}
