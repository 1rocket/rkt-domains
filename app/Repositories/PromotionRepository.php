<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Promotion;

class PromotionRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Promotion::class);
    }
}
