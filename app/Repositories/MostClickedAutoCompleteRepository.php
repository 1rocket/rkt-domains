<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\MostClickedAutoComplete;

class MostClickedAutoCompleteRepository  extends BaseRepository
{
    public function __construct(){
        parent::__construct(MostClickedAutoComplete::class);
    }

    public function findMostClicked($client_id, $limit){
        return $this->model
            ->where('client_id', $client_id)
            ->selectRaw('count(`id`) as count, autocomplete')
            ->groupBy('autocomplete')
            ->orderBy('count', 'desc')
            ->limit($limit)
            ->get();
    }
}