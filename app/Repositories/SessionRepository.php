<?php


namespace RKT\Domains\Repositories;


use Carbon\Carbon;
use RKT\Domains\Models\Session;

class SessionRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Session::class);
    }

    public function findPeopleNow($client_id, $days, $innerJoin, $innerConditions){
        $query = $this->model
            ->where('sessions.client_id', $client_id)
            ->where('sessions.created_at', '>', Carbon::now()->subDays($days));

        if($innerJoin) {
            $query = $query
                ->join($innerJoin, $innerJoin . '.session_id', '=', 'sessions.id')
                ->where($innerConditions);
        }
        return $query->get();
    }
}