<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\CustomerEmail;

class CustomerEmailRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(CustomerEmail::class);
    }

    public function findOrCreate($data){
        $record = null;
        try {
            $record = $this->model->firstOrCreate($data);

            return $record;

        }catch (\Exception $ex){
            throw $ex;
        }
    }
}