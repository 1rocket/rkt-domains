<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\SearchTerm;

class SearchTermRepository extends BaseRepository
{
    public function __construct(){
        parent::__construct(SearchTerm::class);
    }

    public function getProductSearches($conditions, $between){
        return $this->model
            ->selectRaw('count(id) as searches, product_id')
            ->where($conditions)
            ->whereBetween('created_at', $between)
            ->groupBy('product_id')
            ->get();
    }

}