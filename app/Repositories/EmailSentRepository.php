<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\EmailSent;

class EmailSentRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(EmailSent::class);
    }
}