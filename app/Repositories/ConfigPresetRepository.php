<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\ConfigPreset;

class ConfigPresetRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(ConfigPreset::class);
    }
}