<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\VtexClientCategory;

class VtexClientCategoryRepository extends BaseRepository
{
    public function __construct(VtexClientCategory $model)
    {
        parent::__construct($model);
    }

}
