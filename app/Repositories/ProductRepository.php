<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\Product;

class ProductRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(Product::class);
    }

    /***
     * @param $conditions
     * @return boolean
     */
    public function deleteWhereNot($conditions, $client_id)
    {
        return $this->model->where(key($conditions), '!=', $conditions[key($conditions)])
            ->where('client_id', $client_id)
            ->delete();
    }

    public function findAllCategories($client_id){
        return $this->model
            ->where(['client_id' => $client_id, 'status' => true])
            ->where('availability', '>', '0')
            ->select('department')
            ->groupBy('department')
            ->get();
    }

    public function getPagination($client_id, $ids, $limit, $page, $order, $price_range, $availability){
        $query = $this->model
            ->where(['client_id' => $client_id, 'status' => true])
            ->whereIn('id', $ids)
            ->groupBy('p_id')
            ->skip(($limit*$page)-$limit)
            ->take($limit);

        if($availability)
            $query->where('availability', '>', '0');

        if($price_range != null)
            $query->whereBetween('sale_price', [$price_range['min'], $price_range['max']]);

        if(count($order)>1)
            $query->orderBy($order['field'], $order['order']);
        else
            $query->orderByRaw('FIELD(id,\''. implode('\',\'', $ids) .'\')');

        return $query->get();
    }

    public function getAvailabilityProducts($client_id, $ids, $price_range, $availability) {
        $query = $this->model
            ->select('id')
            ->where(['client_id' => $client_id, 'status' => true])
            ->whereIn('id', $ids)
            ->groupBy('p_id');

        if($availability)
            $query->where('availability', '>', '0');

        if($price_range != null)
            $query->whereBetween('sale_price', [$price_range['min'], $price_range['max']]);

        return $query->get();
    }

    public function getMaxAndMinSalePrice($client_id, $ids, $price_range, $availability) {
        $query = $this->model
            ->where(['client_id' => $client_id, 'status' => true])
            ->whereIn('id', $ids);

        if($availability)
            $query->where('availability', '>', '0');

        if($price_range != null)
            $query->whereBetween('sale_price', [$price_range['min'], $price_range['max']]);

        $response['max'] = $query->max('sale_price');
        $response['min'] = $query->min('sale_price');

        return $response;
    }
}
