<?php


namespace RKT\Domains\Repositories;


use RKT\Domains\Models\ClientInfo;

class ClientInfoRepository extends BaseRepository{

    public function __construct(){
        parent::__construct(ClientInfo::class);
    }
}
