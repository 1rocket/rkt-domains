<?php


namespace RKT\Domains\Enums;


abstract class PackageAppType extends Enum
{

    //A ORDENAÇÃO DESTES ITENS INFLUENCIA DIRETAMENTE NO CALCULO DE CONSUMIVEL
    //O CONSUMO DE PACKAGE APP É FEITO AUMENTANDO O CONSUMIVEL DOS ITENS DE FORMA 'ASC'

    //FILE: RKT/Domains/ClientPackageAppRepository.php
    //->orderBy('client_package_apps.type', 'asc')

    const PLAN                      = 1;
    const PROMO                     = 2;
    const BOUGHT                    = 3;
}
