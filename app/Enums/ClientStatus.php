<?php


namespace RKT\Domains\Enums;


abstract class ClientStatus extends Enum
{
    const FREE                     = 1;
    const PREMIUM_MENSAL           = 2;
    const PREMIUM_ANUAL            = 3;
    const PAGAMENTO_PENDENTE       = 10;
}
