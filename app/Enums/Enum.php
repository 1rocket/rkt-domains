<?php


namespace RKT\Domains\Enums;


abstract class Enum
{
    static function getConstants()
    {
        $oClass = new \ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }
}
