<?php


namespace RKT\Domains\Enums;


class IntelligenceType extends Enum
{
    const SELECTORS     = 1;
    const SORTER        = 2;
    const UNIQUE        = 3;
}