<?php


namespace RKT\Domains\Enums;


class PlatformsIds extends Enum
{
    const VTEX              = 1;
    const LOJA_INTEGRADA    = 2;
    const SHOPIFY           = 3;
    const CS_CART           = 4;
    const MAGENTO           = 5;
    const NUVEMSHOP         = 6;
    const TRAY              = 7;
    const WIX               = 8;
    const ECWID             = 9;
    const JET_COMMERCE      = 10;
    const BOX_LOJA          = 11;
    const WOO_COMMERCE      = 12;
    const FBITS             = 13;
    const LINX              = 14;
    const DRUPAL            = 15;
    const OS_COMMERCE       = 16;
    const OCC               = 17;
    const E_COM_CLUB        = 18;
    const D_LOJA_VIRTUAL    = 19;
    const UOL               = 20;
}
