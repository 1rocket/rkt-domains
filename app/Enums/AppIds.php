<?php


namespace RKT\Domains\Enums;


class AppIds extends Enum
{
    const POPUP                     = 1;
    const NOTIFICATION              = 2;
    const EMAILMARKETING            = 3;
    const RECOVERY                  = 4;
    const SHELFS                    = 5;
    const SEARCH                    = 6;
}