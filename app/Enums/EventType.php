<?php


namespace RKT\Domains\Enums;


abstract class EventType extends Enum
{
    const ADM               = 1;
    const SYS               = 2;
}