<?php


namespace RKT\Domains\Enums;


abstract class PageType extends Enum
{
    const HOME                  = 1;
    const PRODUCT               = 2;
    const CART                  = 3;
    const PURCHASE              = 4;
    const CATEGORY              = 5;
    const SEARCH                = 6;
    const PURCHASE_FORM         = 7;
    const OTHERS                = 8;
}