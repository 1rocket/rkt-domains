<?php


namespace RKT\Domains\Enums;


abstract class Intelligence extends Enum
{
    const MOST_VIEWED           = 1;
    const TOP_SELLERS           = 2;
    const MOST_ADDED_TO_CART    = 3;
    const BRAND                 = 4;
    const CATEGORY              = 5;
    const PRICE_RANGE           = 6;
    const REMARKETING_ON_SITE   = 7;
    const SIMILAR               = 8;
    const SMART_HOME            = 9;
    const DISCOUNT              = 10;
    const COMPLEMENTARY_CART    = 11;
    const COMPLEMENTARY_PRODUCT = 12;
    const HIGHER_PRICES         = 13;
    const LOWEST_PRICES         = 14;
    const MANUAL                = 15;
    const NEWS                  = 16;
    const TOP_SEARCHS           = 17;
    const KEYWORD               = 18;
    const SEARCH                = 21;
    const CART_RECOVERY_EMAIL   = 22;
    const MOST_CLICKED          = 23;
    const MOST_RELEVANCE        = 24;
    const BIGGEST_PRICE         = 25;
}
