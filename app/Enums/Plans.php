<?php


namespace RKT\Domains\Enums;


abstract class Plans extends Enum
{
    const FREE                      = 1;
    const MENSAL                    = 2;
    const ANUAL                     = 3;
    const DISABLE                   = 4;
}