<?php


namespace RKT\Domains\Enums;

abstract class Roles extends Enum
{
    const CLIENT                   = 1;
    const ADMIN                    = 2;

}
