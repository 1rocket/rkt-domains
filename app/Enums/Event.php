<?php


namespace RKT\Domains\Enums;


abstract class Event extends Enum
{
    const SIGNUP                    = 1;
    const BOUGHT                    = 2;
    const INSERTORUPDATE            = 3;
    const DELETE                    = 4;
}