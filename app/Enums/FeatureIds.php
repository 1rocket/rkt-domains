<?php


namespace RKT\Domains\Enums;


abstract class FeatureIds extends Enum
{
    const POPUP_RETENTION           =  1;
    const POPUP_SIDESTORE           =  2;
    const POPUP_CATEMAIL            =  3;
    const POPUP_PRODUCTOFTHEDAY     =  4;
    const NOTIFICATION_FEATURE      =  5;
    const EMAIL_REMARKETING         =  6;
    const EMAIL_FIDELIZATION        =  7;
    const EMAIL_BIRTH               =  8;
    const EMAIL_DISCOUNT            =  9;
    const EMAIL_PROMOTION           = 10;
    const RECOVERY_CART             = 11;
    const RECOVERY_BILLET           = 12;
    const SHELF_1                   = 13;
    const SHELF_2                   = 14;
    const SHELF_3                   = 15;
    const SHELF_4                   = 16;
    const SHELF_5                   = 17;
    const SHELF_6                   = 18;
    const SHELF_7                   = 19;
    const SHELF_8                   = 20;
    const SHELF_9                   = 21;
    const SHELF_10                  = 22;
    const SHELF_11                  = 23;
    const SHELF_12                  = 24;
    const SHELF_13                  = 25;
    const SEARCH                    = 26;
    const AUTOCOMPLETE              = 27;
    const DROPDOWN                  = 28;
    const SEARCH_REMARKETING        = 29;

}