<?php


namespace RKT\Domains\Enums;


class PlanType extends Enum
{
    const SIMPLES                      = 1;
    const COMPOSTO                     = 2;
}