<?php


namespace RKT\Domains\Traits;


trait UsesTrait
{
    public function usesTrait($className, $traitName)
    {
        return in_array(
            $traitName,
            array_keys(class_uses($className))
        );
    }
}
