<?php


namespace RKT\Domains\Traits;


trait JWT
{
    protected $headerAlgo = ['typ' => 'JWT', 'alg' => 'HS256'];

    public function generate()
    {
        $header  = $this->header();
        $payload = $this->payload();
        $jwt     = $header . '.' . $payload . '.' . $this->signature($header, $payload);

        return $this->base64Escape($jwt);
    }

    public function check($token)
    {
        $split     = explode('.', $token);
        $header    = $split[0];
        $payload   = $split[1];
        $signature = $split[2];

        return $this->signature($header, $payload) == $signature;
    }

    public function base64Escape($string)
    {
        return str_replace(['+', '/', '='], ['-', '_', ''], $string);
    }

    public function header()
    {
        return base64_encode(json_encode($this->headerAlgo));
    }

    public function payload()
    {
        return base64_encode(json_encode($this->only(['id', 'name', 'email', 'client_id', 'user_roles'])));
    }

    public function signature($header, $payload)
    {
        return base64_encode(hash_hmac('sha256', $header . "." .
            $payload, config('jwt.secret'), true));
    }
}
