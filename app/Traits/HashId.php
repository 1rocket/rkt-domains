<?php


namespace RKT\Domains\Traits;


use Illuminate\Database\Eloquent\Model;

trait HashId
{
    /**
     * set primary key to hash value
     */
    public static function bootHashId()
    {
        static::creating(function (Model $model) {
            $model->{$model->getKeyName()} = hash('md2', microtime());
        });
    }

    public function generateHash()
    {
        return hash('md2', microtime());
    }
}
