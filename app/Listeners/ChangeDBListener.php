<?php

namespace RKT\Domains\Listeners;

use RKT\Domains\Events\ChangeDBEvent;
use RKT\Domains\Enums\Event;

class ChangeDBListener extends BaseListener
{
    protected $repository;

    public function handle(ChangeDBEvent $event){
        $this->setRepositoryInstance($event->getEventLogRepository());

        $dataEventLog['description'] = "Atualização de Informação - " . $event->getChangedModel();
        $dataEventLog['event_id'] = $event->getEventType();
        $dataEventLog['logged_id'] = $event->getChangedId();
        $dataEventLog['logged_table'] = $event->getChangedModel();

        $this->repository->save($dataEventLog);
    }

    private function setRepositoryInstance($instance){
        $this->repository     = (new \ReflectionClass($instance))->newInstance();
    }
}