<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class ConfigPreset extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'status',
        ];

    protected $casts
        = [
            'status' => 'boolean',
        ];


    public function feature_config_presets(){
        return $this->hasMany(FeatureConfigPreset::class);
    }
}
