<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;
use RKT\Domains\Traits\JWT;

class VtexCategoryProductUrl extends Model
{
    use HashId, SoftDeletes, JWT;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'vtex_client_category_id',
            'category_id',
            'full_url',
            'status',
            'init',
            'end',
            'client_id',
        ];

    protected $casts
        = [
            'status'    => 'boolean',
        ];

    public function client()
    {
        return $this->belongsTo(VtexClientCategory::Class);
    }
}
