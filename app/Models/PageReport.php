<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class PageReport extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'count',
            'page',
            'data',
        ];

    protected $casts = [
        'data' => 'array',
    ];
}
