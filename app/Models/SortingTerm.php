<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class SortingTerm extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'term',
            'intelligence_id'
        ];


    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function intelligence(){
        return $this->belongsTo(Intelligence::class);
    }
}
