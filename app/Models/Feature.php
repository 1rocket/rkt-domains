<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Feature extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'app_id',
            'config_preset',
            'html',
            'css',
            'status',
        ];

    protected $casts
        = [
            'status'         => 'boolean',
        ];

    public function app(){
        return $this->belongsTo(App::class);
    }

    public function client_features(){
        return $this->hasMany(ClientFeature::class);
    }

}
