<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'description',
            'event_id',
            'initial',
            'final',
            'duration',
            'type_duration',
            'status',
        ];

    protected $casts
        = [
            'status' => 'boolean',
        ];

    public function clients(){
        return $this->belongsToMany(Client::class)->using(ClientPromotion::class);
    }

    public function clients_promotions(){
        return $this->hasMany(ClientPromotion::class);
    }
}
