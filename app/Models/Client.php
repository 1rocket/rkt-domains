<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'status',
        ];

    protected $casts
        = [
            'status' => 'boolean',
        ];

    public function package_apps(){
        return $this->belongsToMany(PackageApp::class)->using(ClientPackageApp::class);
    }

    public function client_package_apps(){
        return $this->hasMany(ClientPackageApp::class);
    }

    public function plans(){
        return $this->belongsTo(Plan::class)->using(ClientPlan::class);
    }

    public function client_plan(){
        return $this->hasOne(ClientPlan::class);
    }

    public function promotions(){
        return $this->belongsToMany(Promotion::class)->using(ClientPromotion::class);
    }

    public function client_promotions(){
        return $this->hasMany(ClientPromotion::class);
    }

    public function client_features(){
        return $this->hasMany(ClientFeature::class);
    }

    public function client_info(){
        return $this->hasOne(ClientInfo::class);
    }

    public function user(){
        return $this->hasOne(User::class);
    }
}
