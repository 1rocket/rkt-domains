<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Purchase extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'session_id',
            'customer_name',
            'url',
            'platform_purchase_id',
            'total_price',
            'discount',
            'currency',
            'items_count',
        ];

    public function session(){
        return $this->belongsTo(Session::class);
    }
}
