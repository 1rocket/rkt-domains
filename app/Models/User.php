<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;
use Illuminate\Foundation\Auth\User as Authenticatable;
use RKT\Domains\Traits\JWT;

class User extends Authenticatable
{
    use HashId, SoftDeletes, JWT;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'email',
            'password',
            'client_id',
        ];

    protected $hidden
        = [
            'password',
        ];
    public function client()
    {
        return $this->belongsTo(Client::Class);
    }

    public function roles()
    {
        return $this->hasManyThrough(Role::class, UserRole::class, 'user_id', 'id', 'id', 'role_id');
    }

    public function user_roles()
    {
        return $this->hasMany(UserRole::class);
    }

    public function hasRole($role_id)
    {
        foreach ($this->user_roles as $userRole) {
            if ($userRole->role_id == $role_id)
                return true;
        }

        return false;
    }

    static function fromJwt($token)
    {
        $split   = explode('.', $token);
        $payload = json_decode(base64_decode($split[1]));


        return User::with(['roles'])->where('id', $payload->id)->first();
    }
}
