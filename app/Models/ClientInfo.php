<?php

namespace RKT\Domains\Models;


use Illuminate\Database\Eloquent\Model;
use RHV\Domains\Models\ClientConfig;
use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientInfo extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'facebook_id',
            'name',
            'type_update_product',
            'xml_structure_id',
            'url_xml',
            'email',
            'domain',
            'platform_id',
            'platform_config',
            'segment_id',
            'street',
            'state',
            'district',
            'address_number',
            'city',
            'zip_code',
            'complement',
            'cnpj',
            'razao_social',
            'fantasy_name',
            'phone_number',
            'cell_number',
            'other_email',
            'other_contact',
            'preferred_contact',
            'email_confirmed',
            'hosts',
            'account-name',
            'token',
            'app-key',
            'url-store',
        ];

    protected $casts = [
        'email_confirmed'   => 'boolean',
        'hosts'             => 'array',
        'platform_config'   => 'array',
    ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function platform(){
        return $this->belongsTo(Platform::class);
    }

    public function segment(){
        return $this->belongsTo(Segment::class);
    }

    public function xml_structure(){
        return $this->belongsTo(XmlStructure::class);
    }

    static function isValidHost($host, $client_id){
        return ClientInfo::where('hosts->' . $host, true)
            ->where('client_id', $client_id)
            ->first();
    }
}
