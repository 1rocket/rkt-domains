<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Intelligence extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'id',
            'name',
            'internal_name',
            'description',
            'type',
            'searchable',
            'status',
        ];

    protected $casts = [
        'status'      => 'boolean',
        'searchable'  => 'boolean'
    ];
}
