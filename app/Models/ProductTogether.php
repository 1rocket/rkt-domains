<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class ProductTogether extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'product_id',
            'bought',
            'carts',
            'views',
        ];

    protected $casts
        = [
            'bought' => 'array',
            'carts' => 'array',
            'views' => 'array',
        ];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
