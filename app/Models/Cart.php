<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Cart extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'session_id',
            'url',
            'platform_cart_id',
            'total_price',
            'currency',
            'items_count',
        ];

    public function session(){
        return $this->belongsTo(Session::class);
    }
}
