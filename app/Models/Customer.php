<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Customer extends Model
{
    use SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'id',
        ];

    public function sessions(){
        return $this->hasMany(Session::class);
    }

    public function emails(){
        return $this->belongsToMany(Email::class)->using(CustomerEmail::class);
    }

    public function customer_emails(){
        return $this->hasMany(CustomerEmail::class);
    }

    public function customer_datas(){
        return $this->hasMany(CustomerData::class);
    }
}
