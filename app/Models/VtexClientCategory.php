<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;
use RKT\Domains\Traits\JWT;
use function foo\func;

class VtexClientCategory extends Model
{
    use HashId, SoftDeletes, JWT;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'category_id',
            'category_name',
            'client_id',
            'products_quantity',
            'products_in_base',
        ];

    public function client()
    {
        return $this->belongsTo(Client::Class);
    }

}
