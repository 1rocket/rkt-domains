<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class RolePermission extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'role_id',
        'permission_id',
    ];

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function permissions(){
        return $this->belongsTo(Permission::class);
    }
}
