<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Session extends Model
{
    use SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'id',
            'client_id',
            'customer_id',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function purchases(){
        return $this->hasMany(Purchase::class);
    }

    public function carts(){
        return $this->hasMany(Cart::class);
    }
    public function page_views(){
        return $this->hasMany(PageView::class);
    }
}
