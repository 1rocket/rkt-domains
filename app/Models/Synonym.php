<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Synonym extends Model
{
    use HashId,SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'synonym_term',
            'synonym_origin',
            'term',
            'term_origin',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }
}
