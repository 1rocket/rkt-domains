<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Role extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable =[
        'description'
    ];

    public function users(){
        return $this->belongsToMany(User::Class)->using(UserRole::Class);
    }

    public function role_permissions(){
        return $this->hasMany(RolePermission::class);
    }

    public function permissions(){
        return $this->hasManyThrough(Permission::class, RolePermission::class, 'role_id', 'id', 'id', 'permission_id');
    }
}
