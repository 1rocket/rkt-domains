<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class FeatureReport extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_feature_id',
            'impressions',
            'clicks',
            'conversion',
            'sales',
            'open_rate',
            'page',
            'products',
        ];

    protected $casts = [
        'products' => 'array',
        'page' => 'array',
    ];
}
