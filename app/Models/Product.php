<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Product extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'p_id',
            'sku',
            'title',
            'type',
            'description',
            'price',
            'sale_price',
            'discount',
            'reference_id',
            'department',
            'category',
            'subcategory',
            'tags',
            'brand',
            'mpn',
            'gtin',
            'link',
            'image_link',
            'image_link_2',
            'months',
            'amount',
            'availability',
            'request_update_time',
            'variation',
            'status',
        ];

    protected $casts
        = [
            'variation'             => 'array',
            'status'                => 'boolean',
            'request_update_time'   => 'timestamp',
            'price'                 => 'float',
            'sale_price'            => 'float',
            'discount'              => 'float',
            'amount'                => 'float',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function product_metrics(){
        return $this->hasMany(ProductMetric::class);
    }
}
