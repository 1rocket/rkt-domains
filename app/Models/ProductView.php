<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class ProductView extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'session_id',
            'url',
            'product_id',
            'p_id',
        ];

    public function session(){
        return $this->belongsTo(Session::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
