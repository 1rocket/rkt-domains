<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class EmailSent extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'client_id',
        'widget_id',
        'intelligence_id',
        'email',
        'sender',
        'content',
        'subject',
        'open_count',
        'click_count',
        'send_date',
        'view_date',
        'click_date',
        'status',
        'isTest',
    ];

    protected $casts
        = [
            'status' => 'boolean',
            'isTest' => 'boolean',
        ];

    protected $dates = [
        'send_date',
        'open_date',
        'click_date',
    ];
}
