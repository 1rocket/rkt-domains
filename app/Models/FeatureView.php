<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class FeatureView extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'client_feature_id',
            'page',
            'products',
        ];

    protected $casts = [
        'products' => 'array',
    ];

    public function client_feature(){
        return $this->belongsTo(ClientFeature::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }
}
