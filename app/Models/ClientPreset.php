<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class ClientPreset extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'config_preset_id',
            'client_feature_id',
        ];

    protected $casts
        = [
            'status' => 'boolean',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function config_preset(){
        return $this->belongsTo(ConfigPreset::class);
    }
}
