<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class FeatureTheme extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'client_id',
            'app_id',
            'html',
            'css',
            'status',
        ];

    protected $casts
        = [
            'status'    => 'boolean',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }
}
