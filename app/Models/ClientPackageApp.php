<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientPackageApp extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'package_app_id',
            'consumed',
            'unlimited',
            'type',
            'expire',
            'status',
        ];

    protected $casts
        = [
            'status'    => 'boolean',
            'unlimited' => 'boolean',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function package_app(){
        return $this->belongsTo(PackageApp::class);
    }
}
