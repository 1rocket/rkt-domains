<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Permission extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'name',
        'route',
        'status',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    public function role_permissions(){
        return $this->hasMany(RolePermission::class);
    }
}
