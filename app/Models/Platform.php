<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Platform extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'config',
            'status',
            'api_status'
        ];

    protected $casts
        = [
            'status' => 'boolean',
            'api_status' => 'boolean',
            'config' => 'array',
        ];
}
