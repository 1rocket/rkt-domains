<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Api\Services\ClientFeatureService;
use RKT\Domains\Traits\HashId;

class ClientSearchFilter extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'filters',
        ];

    protected $casts
        =[
            'filters' => 'array',
        ];
    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function user(){
        return $this->hasOne(User::class);
    }
}
