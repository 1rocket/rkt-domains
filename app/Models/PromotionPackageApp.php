<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionPackageApp extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'promotion_id',
            'package_app_id',
            'period',
            'quantity',
            'unlimited',
            'real',
            'dollar',
            'euro',
        ];

    protected $casts = [
        'unlimited' => 'boolean',
    ];

    public function package_app(){
        return $this->belongsTo(PackageApp::class);
    }

    public function promotion(){
        return $this->belongsTo(Promotion::class);
    }
}
