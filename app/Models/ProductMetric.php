<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class ProductMetric extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'product_id',
            'price',
            'views',
            'carts',
            'purchases',
            'searches',
            'loveds',
            'dislikes',
            'trashs',
            'copies',
            'department',
            'p_id',
            'sku',
            'brand',
            'client_id',
        ];

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
