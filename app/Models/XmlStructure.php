<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class XmlStructure extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = [
        'type_structure',
        'item',
        'p_id',
        'sku',
        'title',
        'description',
        'price',
        'sale_price',
        'discount',
        'reference_id',
        'department',
        'category',
        'subcategory',
        'tags',
        'brand',
        'mpn',
        'gtin',
        'link',
        'image_link',
        'image_link_2',
        'months',
        'amount',
        'availability',
        'variation',
    ];

    public function client_info(){
        return $this->hasMany(ClientInfo::class);
    }
}
