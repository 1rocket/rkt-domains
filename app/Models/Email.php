<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class Email extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'email',
        ];

    public function customers(){
        return $this->belongsToMany(Customer::class)->using(CustomerEmail::class);
    }

    public function customer_emails(){
        return $this->hasMany(CustomerEmail::class);
    }
}
