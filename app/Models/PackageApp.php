<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageApp extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'real',
            'dollar',
            'euro',
            'app_id',
            'quantity',
            'unlimited',
        ];

    protected $casts
        = [
            'unlimited' => 'boolean',
        ];

    public function clients(){
        return $this->belongsToMany(Client::class)->using(ClientPackageApp::class);
    }

    public function client_package_apps(){
        return $this->hasMany(ClientPackageApp::class);
    }

    public function app(){
        return $this->belongsTo(App::class);
    }

    public function plan_package_apps(){
        return $this->hasMany(PlanPackageApp::class);
    }

    public function promotion_package_apps(){
        return $this->hasMany(PromotionPackageApp::class);
    }
}
