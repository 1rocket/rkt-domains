<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class CustomerData extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'id',
            'customer_id',
            'platform_customer_id',
            'first_name',
            'last_name',
            'country_code',
            'province_code',
            'city',
            'address',
            'zip',
            'phone',
            'phone2',
            'ip',
            'client_type',
            'client_name',
            'client_version',
            'client_engine',
            'client_engineVersion',
            'os_name',
            'os_version',
            'os_platform',
            'device_type',
            'device_brand',
            'device_model',
            'device_resolution',
            'bot',
        ];

    public function customer(){
        return $this->belongsTo(Customer::class);
    }
}
