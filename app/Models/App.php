<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class App extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'description',
            'quick_text',
            'logo',
            'images',
            'videos'.
            'status',
        ];

    protected $casts
        = [
            'images' => 'array',
            'videos' => 'array',
            'status' => 'boolean',
        ];

    public function package_apps(){
        return $this->hasMany(PackageApp::class);
    }

    public function features(){
        return $this->hasMany(Feature::class);
    }
}
