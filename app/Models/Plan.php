<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'type',
            'real',
            'euro',
            'dollar',
            'duration',
            'type_duration',
            'status',
        ];

    protected $casts
        = [
            'status' => 'boolean',
        ];

    public function clients(){
        return $this->belongsToMany(Client::class)->using(ClientPlan::class);
    }
    public function client_plans(){
        return $this->hasMany(ClientPlan::class);
    }

    public function package_apps(){
        return $this->belongsToMany(PackageApp::class)->using(PlanPackageApp::class);
    }
    public function plan_package_apps(){
        return $this->hasMany(PlanPackageApp::class);
    }
}
