<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanPackageApp extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'plan_id',
            'package_app_id',
        ];

    protected $casts = [
        'images' => 'array',
        'videos' => 'array'
    ];

    public function package_app(){
        return $this->belongsTo(PackageApp::class);
    }

    public function plan(){
        return $this->belongsTo(Plan::class);
    }
}
