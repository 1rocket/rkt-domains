<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class CustomerEmail extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'email_id',
            'customer_id'
        ];

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function emails(){
        return $this->belongsToMany(Email::class);
    }
}
