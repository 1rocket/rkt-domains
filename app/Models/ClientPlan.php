<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientPlan extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'plan_id',
            'duration',
            'status',
        ];

    protected $casts
        = [
            'status' => 'boolean',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function plan(){
        return $this->belongsTo(Plan::class);
    }
}
