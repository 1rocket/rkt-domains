<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class FeatureConfigPreset extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'name',
            'config_preset_id',
            'feature_id',
            'config',
            'feature_theme_id',
            'status',
        ];

    protected $casts
        = [
            'config'    => 'array',
            'status'    => 'boolean',
        ];


    public function feature_theme(){
        return $this->belongsTo(FeatureTheme::class);
    }
    public function config_presets(){
        return $this->belongsTo(ConfigPreset::class);
    }
}
