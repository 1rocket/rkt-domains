<?php

namespace RKT\Domains\Models;

use RKT\Domains\Traits\HashId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientPromotion extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'client_id',
            'promotion_id',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function promotion(){
        return $this->belongsTo(Promotion::class);
    }
}
