<?php

namespace RKT\Domains\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RKT\Domains\Traits\HashId;

class ClientFeature extends Model
{
    use HashId, SoftDeletes;
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable
        = [
            'feature_id',
            'client_id',
            'config',
            'feature_theme_id',
            'status',
        ];

    protected $casts
        = [
            'status' => 'boolean',
            'config' => 'array',
        ];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function feature(){
        return $this->belongsTo(Feature::class);
    }

    public function feature_theme(){
        return $this->belongsTo(FeatureTheme::class);
    }

}
