<?php

use Illuminate\Database\Seeder;

class FeatureThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $htmlPopUp = '';
        $htmlNotification = '';
        $htmlEmailMarketing = '';
        $htmlRecovery = '';
        $htmlShelfs = '';
        $htmlSearch = '';

        $cssPopUp = '';
        $cssNotification = '';
        $cssEmailMarketing = '';
        $cssRecovery = '';
        $cssShelfs = '';
        $cssSearch = '';

        \DB::table('feature_themes')->insert([
            ['id'=> '1', 'name' => 'Default', 'app_id' => \RKT\Domains\Enums\AppIds::POPUP,          'client_id' => '0', 'html' => $htmlPopUp,           'css' => $cssPopUp, 'status' => true],
            ['id'=> '2', 'name' => 'Default', 'app_id' => \RKT\Domains\Enums\AppIds::NOTIFICATION,   'client_id' => '0', 'html' => $htmlNotification,    'css' => $cssNotification, 'status' => true],
            ['id'=> '3', 'name' => 'Default', 'app_id' => \RKT\Domains\Enums\AppIds::EMAILMARKETING, 'client_id' => '0', 'html' => $htmlEmailMarketing,  'css' => $cssEmailMarketing, 'status' => true],
            ['id'=> '4', 'name' => 'Default', 'app_id' => \RKT\Domains\Enums\AppIds::RECOVERY,       'client_id' => '0', 'html' => $htmlRecovery,        'css' => $cssRecovery, 'status' => true],
            ['id'=> '5', 'name' => 'Default', 'app_id' => \RKT\Domains\Enums\AppIds::SHELFS,         'client_id' => '0', 'html' => $htmlShelfs,          'css' => $cssShelfs, 'status' => true],
            ['id'=> '6', 'name' => 'Default', 'app_id' => \RKT\Domains\Enums\AppIds::SEARCH,         'client_id' => '0', 'html' => $htmlSearch,          'css' => $cssSearch, 'status' => true],
        ]);
    }
}
