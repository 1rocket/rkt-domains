<?php

use Illuminate\Database\Seeder;

class IntelligencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('intelligences')->insert([
            ['id' => 1, 'type' => 'order', 'dashboard_name' => 'Mais Vistos', 'name' => 'MOST_VIEWED', 'description' => 'Ordena os produtos pelos mais clicados no site do cliente', 'status' => 1],
            ['id' => 2, 'type' => 'order', 'dashboard_name' => 'Mais Vendidos', 'name' => 'TOP_SELLERS', 'description' => 'Ordena os produtos pelos mais vendidos no site do cliente', 'status' => 1],
            ['id' => 3, 'type' => 'order', 'dashboard_name' => 'Mais Adicionados Ao Carrinho', 'name' => 'MOST_ADDED_TO_CART', 'description' => 'Ordena os produtos pelos mais adicionados ao carrinho no site do cliente', 'status' => 1],
            ['id' => 4, 'type' => 'select', 'dashboard_name' => 'Marca', 'name' => 'BRAND', 'description' => 'Retorna produtos de uma marca específica cadastrado pelo cliente ou dinamicamente quando adicionada a página de produto', 'status' => 1],
            ['id' => 5, 'type' => 'select', 'dashboard_name' => 'Categoria', 'name' => 'CATEGORY', 'description' => 'Retorna produtos de uma categoria específica cadastrado pelo cliente ou dinamicamente quando adicionada a página de produto', 'status' => 1],
            ['id' => 6, 'type' => 'select', 'dashboard_name' => 'Faixa de Preço', 'name' => 'PRICE_RANGE', 'description' => 'Retorna os produtos com o preço entre os valores cadastrados pelo cliente', 'status' => 1],
            ['id' => 7, 'type' => 'select', 'dashboard_name' => 'Remarketing On Site', 'name' => 'REMARKETING_ON_SITE', 'description' => 'Retorna os produtos navegados pelo usuário, por padrão deve retornar em ordem de navegação', 'status' => 1],
            ['id' => 8, 'type' => 'select', 'dashboard_name' => 'Similar', 'name' => 'SIMILAR', 'description' => 'Retorna os produtos com nome similar ao visitado pelo usuário, podendo pegar os dados de navegação anterior caso não esteja na página de produto', 'status' => 1],
            ['id' => 10, 'type' => 'order', 'dashboard_name' => 'Melhores Descontos', 'name' => 'BEST_DISCOUNTS', 'description' => 'Ordena os produtos com o maior desconto no site do cliente', 'status' => 1],
            ['id' => 13, 'type' => 'order', 'dashboard_name' => 'Maior Preço', 'name' => 'HIGHER_PRICES', 'description' => 'Ordena os produtos pelo maior preço', 'status' => 1],
            ['id' => 14, 'type' => 'order', 'dashboard_name' => 'Menor Preço', 'name' => 'LOWEST_PRICES', 'description' => 'Ordena os produtos pelo menor preço', 'status' => 1],
            ['id' => 16, 'type' => 'order', 'dashboard_name' => 'Mais Novos', 'name' => 'NEWEST', 'description' => 'Ordena os produtos pela data de adição', 'status' => 1],
            ['id' => 17, 'type' => 'order', 'dashboard_name' => 'Mais Pesquisados', 'name' => 'TOP_SEARCHS', 'description' => 'Ordena os produtos pelos mais clicados na busca', 'status' => 1],
            ['id' => 18, 'type' => 'select', 'dashboard_name' => 'Palavra Chave', 'name' => 'KEYWORD', 'description' => 'Retorna os produtos que possuem uma palavra-chave cadastrada pelo cliente', 'status' => 1],
        ]);
    }
}
