<?php

use Illuminate\Database\Seeder;

class ConfigPresetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('config_presets')->insert([
            ['id' => '1', 'name' => 'Cadastro', 'status' => true],
        ]);
    }
}
