<?php

use Illuminate\Database\Seeder;

class PlatformTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('platforms')->insert([
            ['id' => \RKT\Domains\Enums\PlatformsIds::VTEX,             'config' => json_encode([
                                                                                        'appToken' => true,
                                                                                        'accountName' => true,
                                                                                        'appKey' => true,
                                                                                        'urlStore' => true], true),
                                                                        'name' => 'VTEX', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::LOJA_INTEGRADA,   'config' => json_encode(['urlStore' => true], true), 'name' => 'Loja Integrada', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::SHOPIFY,          'config' => json_encode(['urlStore' => true], true), 'name' => 'Shopify', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::CS_CART,          'config' => json_encode(['urlStore' => true], true), 'name' => 'CS Cart', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::MAGENTO,          'config' => json_encode(['urlStore' => true], true), 'name' => 'Magento', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::NUVEMSHOP,        'config' => json_encode(['urlStore' => true], true), 'name' => 'Nuvem Shop', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::TRAY,             'config' => json_encode(['urlStore' => true], true), 'name' => 'Tray', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::WIX,              'config' => json_encode(['urlStore' => true], true), 'name' => 'Wix', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::ECWID,            'config' => json_encode(['urlStore' => true], true), 'name' => 'Ecwid', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::JET_COMMERCE,     'config' => json_encode(['urlStore' => true], true), 'name' => 'Jet Commerce', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::BOX_LOJA,         'config' => json_encode(['urlStore' => true], true), 'name' => 'Box Loja', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::WOO_COMMERCE,     'config' => json_encode(['urlStore' => true], true), 'name' => 'Woo Commerce', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::FBITS,            'config' => json_encode(['urlStore' => true], true), 'name' => 'FBits', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::LINX,             'config' => json_encode(['urlStore' => true], true), 'name' => 'Linx', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::DRUPAL,           'config' => json_encode(['urlStore' => true], true), 'name' => 'Drupal', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::OS_COMMERCE,      'config' => json_encode(['urlStore' => true], true), 'name' => 'OS Commerce', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::OCC,              'config' => json_encode(['urlStore' => true], true), 'name' => 'OCC', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::E_COM_CLUB,       'config' => json_encode(['urlStore' => true], true), 'name' => 'ECOM Club', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::D_LOJA_VIRTUAL,   'config' => json_encode(['urlStore' => true], true), 'name' => 'D Loja Virtual', 'status' => true],
            ['id' => \RKT\Domains\Enums\PlatformsIds::UOL,              'config' => json_encode(['urlStore' => true], true), 'name' => 'UOL', 'status' => true],

        ]);
    }
}
