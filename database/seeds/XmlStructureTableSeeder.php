<?php

use Illuminate\Database\Seeder;

class XmlStructureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('xml_structures')->insert([
            [
                'id'=> 1,
                'type_structure' => 'rss',
                'item' => 'channel->item',
                'p_id' => 'id',
                'sku' => 'sku',
                'title' => 'title',
                'description' => 'description',
                'price' => 'price',
                'sale_price' => 'sale_price',
                'discount' => NULL,
                'reference_id' => 'reference_id',
                'department' => 'department',
                'category' => 'product_type',
                'subcategory' => NULL,
                'tags' => NULL,
                'brand' => 'brand',
                'mpn' => 'mpn',
                'gtin' => 'gtin',
                'link' => 'link',
                'image_link' => 'image_link',
                'image_link_2' => NULL,
                'months' => 'installment->months',
                'amount' => 'installment->amount',
                'availability' => 'availability',
                'variation' => null,
            ],
            [
                'id'=> 2,
                'type_structure' => 'atom',
                'item' => 'entry',
                'p_id' => 'id',
                'sku' => 'sku',
                'title' => 'title',
                'description' => 'description',
                'price' => 'price',
                'sale_price' => 'sale_price',
                'discount' => NULL,
                'reference_id' => 'reference_id',
                'department' => 'department',
                'category' => 'product_type',
                'subcategory' => NULL,
                'tags' => NULL,
                'brand' => 'brand',
                'mpn' => 'mpn',
                'gtin' => 'gtin',
                'link' => 'link',
                'image_link' => 'image_link',
                'image_link_2' => NULL,
                'months' => 'installment->months',
                'amount' => 'installment->amount',
                'availability' => 'availability',
                'variation' => null,
            ],
            [
                'id'=> 3,
                'type_structure' => 'loja_integrada',
                'item' => 'channel->item',
                'p_id' => 'item_group_id',
                'sku' => 'id',
                'title' => 'title',
                'description' => 'description',
                'price' => 'price',
                'sale_price' => 'sale_price',
                'discount' => NULL,
                'reference_id' => NULL,
                'department' => 'product_type',
                'category' => NULL,
                'subcategory' => NULL,
                'tags' => NULL,
                'brand' => 'brand',
                'mpn' => 'mpn',
                'gtin' => 'gtin',
                'link' => 'link',
                'image_link' => 'image_link',
                'image_link_2' => NULL,
                'months' => 'installment->months',
                'amount' => 'installment->amount',
                'availability' => 'availability',
                'variation' => null,
            ]
        ]);
    }
}
