<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(PlanTableSeeder::class);
        $this->call(AppTableSeeder::class);
        $this->call(EventTableSeeder::class);
        $this->call(PackageAppTableSeeder::class);
        $this->call(PlanPackageAppTableSeeder::class);
        $this->call(PromotionPackageAppTableSeeder::class);
        $this->call(PromotionTableSeeder::class);
        $this->call(FeatureTableSeeder::class);
        $this->call(XmlStructureTableSeeder::class);
        $this->call(FeatureConfigPresetTableSeeder::class);
        $this->call(ConfigPresetTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RolePermissionTableSeeder::class);
        $this->call(PlatformTableSeeder::class);
        $this->call(FeatureThemeSeeder::class);
    }
}
