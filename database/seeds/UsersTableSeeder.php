<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            ['id'=> 1, 'name' => 'Lucas Nolasco',     'email' => 'lucas.nolasco@roihero.com.br',  'client_id' => '0', 'password' => \Illuminate\Support\Facades\Hash::make('roihero@123#')],
            ['id'=> 2, 'name' => 'Moises Dourado',    'email' => 'moises.dourado@roihero.com.br', 'client_id' => '0', 'password' => \Illuminate\Support\Facades\Hash::make('roihero@123#')],
            ['id'=> 3, 'name' => 'Eliabe Franca',     'email' => 'eliabe.franca@roihero.com.br',  'client_id' => '0', 'password' => \Illuminate\Support\Facades\Hash::make('roihero@123#')],
        ]);
    }
}
