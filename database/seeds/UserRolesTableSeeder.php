<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_roles')->insert([
            ['id'=> hash('md2', microtime()), 'user_id' => '1', 'role_id' => '2'],
            ['id'=> hash('md2', microtime()), 'user_id' => '2', 'role_id' => '2'],
            ['id'=> hash('md2', microtime()), 'user_id' => '3', 'role_id' => '2'],
        ]);
    }
}
