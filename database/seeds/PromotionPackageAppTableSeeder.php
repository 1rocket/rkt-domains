<?php

use Illuminate\Database\Seeder;

class PromotionPackageAppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('promotion_package_apps')->insert([
            ['id' => hash('md2', microtime()), 'promotion_id' => '1', 'package_app_id' => 1, 'quantity' => 70000, 'real' => 0.00, 'dollar' => 0.00, 'euro' => 0.00, 'period' => 7],
        ]);
    }
}
