<?php

use Illuminate\Database\Seeder;

class PackageAppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('package_apps')->insert([
            ['id' => 1, 'real' => 0.00, 'dollar' => 0.00, 'euro' => 0.00, 'app_id' => 1, 'quantity' => 10000],
            ['id' => 2, 'real' => 0.00, 'dollar' => 0.00, 'euro' => 0.00, 'app_id' => 2, 'quantity' => 20000],
            ['id' => 3, 'real' => 0.00, 'dollar' => 0.00, 'euro' => 0.00, 'app_id' => 3, 'quantity' => 30000],
            ['id' => 4, 'real' => 0.00, 'dollar' => 0.00, 'euro' => 0.00, 'app_id' => 4, 'quantity' => 40000],
            ['id' => 5, 'real' => 0.00, 'dollar' => 0.00, 'euro' => 0.00, 'app_id' => 5, 'quantity' => 50000],
            ['id' => 6, 'real' => 0.00, 'dollar' => 0.00, 'euro' => 0.00, 'app_id' => 6, 'quantity' => 60000],
        ]);
    }
}
