<?php

use Illuminate\Database\Seeder;

class AppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('apps')->insert([

            //id sem hash
            ['id' => \RKT\Domains\Enums\AppIds::POPUP,          'name' => 'Pop-Up',                   'status' => true, 'quick_text' => '', 'description' => '', 'logo' => '', 'videos' => json_encode(['urls' => []]), 'images' => json_encode(['urls' => []])],
            ['id' => \RKT\Domains\Enums\AppIds::NOTIFICATION,   'name' => 'Notificações',             'status' => true, 'quick_text' => '', 'description' => '', 'logo' => '', 'videos' => json_encode(['urls' => []]), 'images' => json_encode(['urls' => []])],
            ['id' => \RKT\Domains\Enums\AppIds::EMAILMARKETING, 'name' => 'Email Marketing',          'status' => true, 'quick_text' => '', 'description' => '', 'logo' => '', 'videos' => json_encode(['urls' => []]), 'images' => json_encode(['urls' => []])],
            ['id' => \RKT\Domains\Enums\AppIds::RECOVERY,       'name' => 'Recuperação',              'status' => true, 'quick_text' => '', 'description' => '', 'logo' => '', 'videos' => json_encode(['urls' => []]), 'images' => json_encode(['urls' => []])],
            ['id' => \RKT\Domains\Enums\AppIds::SHELFS,         'name' => 'Vitrines',                 'status' => true, 'quick_text' => '', 'description' => '', 'logo' => '', 'videos' => json_encode(['urls' => []]), 'images' => json_encode(['urls' => []])],
            ['id' => \RKT\Domains\Enums\AppIds::SEARCH,         'name' => 'Barra de Busca',           'status' => true, 'quick_text' => '', 'description' => '', 'logo' => '', 'videos' => json_encode(['urls' => []]), 'images' => json_encode(['urls' => []])],
        ]);
    }
}
