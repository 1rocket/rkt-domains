<?php

use Illuminate\Database\Seeder;

class FeatureConfigPresetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages_enabled_array = [
            'home' => true,
            'product' => true,
            'cart' => true,
            'purchase' => true,
            'category' => true,
            'search' => true,
            'purchase_form' => true,
            'others' => true
        ];

        $pages_disabled_array = [
            'home' => false,
            'product' => false,
            'cart' => false,
            'purchase' => false,
            'category' => false,
            'search' => false,
            'purchase_form' => false,
            'others' => false
        ];

        $pages_only_product = [
            'home' => false,
            'product' => true,
            'cart' => false,
            'purchase' => false,
            'category' => false,
            'search' => false,
            'purchase_form' => false,
            'others' => false,
        ];

        $pages_only_home = [
            'home' => false,
            'product' => true,
            'cart' => false,
            'purchase' => false,
            'category' => false,
            'search' => false,
            'purchase_form' => false,
            'others' => false,
        ];


        // popup retenção
        $config1 = [
            'color'         => '#fff',
            'title'         => 'Não Vá Embora Ainda!',
            'banner_path'   => '',
            'banner_link'   => '',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 1,
            'pages'         => $pages_enabled_array
        ];
        // popup loja lateral
        $config2 = [
            'color' => '#fff',
            'title' => 'Sua Loja',
            'position' => 'left',
            'banner_path' => '',
            'banner_link' => '',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
            'pages' => $pages_enabled_array
        ];
        // popup captura de email
        $config3 = [
            'color' => '#fff',
            'title' => 'Receba Nossas Ofertas!',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 1,
            'pages' => $pages_enabled_array
        ];
        // popup produto do dia
        $config4 = [
            'color' => '#fff',
            'title' => 'Produto do Dia',
            'position' => 'bottom_left',
            'regressive_counter' => false,
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 1,
            'pages' => $pages_enabled_array
        ];
        // notificações
        $config5 = [
            'color'              => '#fff',
            'position'           => 'bottom_right',
            'quantity'           => 1,
            'pages'              => $pages_enabled_array,
            'cycles'             => 1,
            'people_now_home_days'      => 1,
            'people_now_product_days'   => 1,
            'people_now_cart_days'      => 1,
            'people_now_category_days'  => 1,
            'product_most_viewed_days'  => 7,
            'people_bought_days'        => 7,
            'people_discount_days'      => 7,
            'people_cart_days'          => 7,
            'duration'           => 5,
            'delay'              => 10,
            'people_now_value'      => 0,
            'people_bought_value' 	=> 0,
            'people_discount_value' => 0,
            'people_cart_value'     => 0,
            'notifications'      => [
                'home' => [
                    '{{PEOPLE_NOW}} pessoas estão navegando agora.',
                    '{{CUSTOMER_NAME}}, acabou de adicionar o produto {{PRODUCT_NAME}} no carrinho.',
                    '{{PEOPLE_BOUGHT}} pessoas compraram nessa loja nos últimos dias.',
                    'O produto {{PRODUCT_NAME}} foi adicionado ao carrinho.',
                    '{{PEOPLE_DISCOUNT}} pessoas estão aproveitando os descontos.',
                    'O produto {{PRODUCT_NAME}} acabou de ser adicionado ao carrinho.',
                    '{{CUSTOMER_NAME}} comprou o produto {{PRODUCT_NAME}}.',
                    'O estoque de alguns itens estão esgotando, compre agora!',
                    '{{CUSTOMER_NAME}} comprou o produto {{PRODUCT_NAME}}.',
                    'O produto {{PRODUCT_NAME}} acabou de ser adicionado ao carrinho.',
                    '{{PEOPLE_CART}} pessoas adicionaram produtos ao carrinho recentemente.',
                ],
                'category' => [
                    '{{PEOPLE_NOW}} pessoas estão navegando agora.',
                    '{{CUSTOMER_NAME}}, acabou de adicionar o produto {{PRODUCT_NAME}} no carrinho.',
                    '{{PEOPLE_BOUGHT}} pessoas compraram nessa loja nos últimos dias.',
                    'O produto {{PRODUCT_NAME}} foi adicionado ao carrinho.',
                    '{{PEOPLE_DISCOUNT}} pessoas estão aproveitando os descontos.',
                    'O produto {{PRODUCT_NAME}} acabou de ser adicionado ao carrinho.',
                    '{{CUSTOMER_NAME}} comprou o produto {{PRODUCT_NAME}}.',
                    'O estoque de alguns itens estão esgotando, compre agora!',
                    '{{CUSTOMER_NAME}} comprou o produto {{PRODUCT_NAME}}.',
                    'O produto {{PRODUCT_NAME}} acabou de ser adicionado ao carrinho.',
                    '{{PEOPLE_CART}} pessoas adicionaram produtos ao carrinho recentemente.',
                ],
                'product' => [
                    '{{PEOPLE_NOW}} pessoas visualizaram este produto recentemente.',
                    '{{CUSTOMER_NAME}}, acabou de adicionar este produto ao carrinho.',
                    '{{PEOPLE_BOUGHT}} pessoas compraram nessa loja nos últimos dias.',
                    '{{PEOPLE_DISCOUNT}} pessoas estão aproveitando os descontos.',
                    'O estoque deste produto pode esgotar rapidamente, aproveite agora!',
                ],
                'cart'  => [
                    '{{PEOPLE_NOW}} pessoas adicionaram este produto recentemente.',
                    '{{CUSTOMER_NAME}}, acabou de adicionar este produto ao carrinho.',
                    '{{PEOPLE_BOUGHT}} pessoas compraram nessa loja nos últimos dias.',
                    '{{PEOPLE_DISCOUNT}} pessoas estão aproveitando os descontos.',
                    'O estoque deste produto pode esgotar rapidamente, aproveite agora!',
                ]
            ],
            'mapping' => [
                'home'      => [],
                'category'  => [],
                'product'   => [],
                'cart'      => [],
            ]
        ];
        // email marketing remarketing
        $config6 = [
            'title' => 'Ofertas Especiais',
            'text' => 'Não perca essa oportunidade, compre agora!',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
            'banner_path' => '',
            'banner_link' => ''

        ];
        // email marketing fidelizacao
        $config7 = [
            'title' => 'Os produtos que você queria, estão aqui!',
            'text' => 'Não perca essa oportunidade, compre agora!',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
            'banner_path' => '',
            'banner_link' => ''
        ];
        // email marketing aniversario
        $config8 = [
            'title' => 'Feliz Aniversário!',
            'text' => 'Separamos algumas ofertas para você nesta data especial!',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
            'banner_path' => '',
            'banner_link' => '',
            'coupon'=> ''
        ];
        // email marketing cupom
        $config9 = [
            'title' => 'Hoje tem cupom especial pra você!',
            'text' => 'COrre logo pra aproveitar',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
            'banner_path' => '',
            'banner_link' => '',
            'coupon'=> ''
        ];
        // email marketing promocao
        $config10 = [
            'title' => 'Olha estes descontos incríveis!',
            'text' => 'Não perca essa oportunidade, compre agora!',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
            'banner_path' => '',
            'banner_link' => ''
        ];
        // email marketing carrinho
        $config11 = [
            'title' => 'Você esqueceu de algo',
            'text' => 'Corra para completar a sua compra enquanto os produtos ainda estão em estoque',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
            'banner_path' => '',
            'banner_link' => '',
            'coupon'=> ''
        ];
        $config12 = [
            'color' => '#fff',
            'title' => '',
            'intelligences' => [\RKT\Domains\Enums\Intelligence::MOST_VIEWED],
            'quantity'      => 12,
        ];
        // SHELFS
        $config13 = [
            'color_title' => '#fff',
            'title' => 'Baixou de Preço',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_home
        ];
        $config14 = [
            'color_title' => '#fff',
            'title' => 'Outros clientes estão vendo',
            'quantity' => 4,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_home
        ];
        $config15 = [
            'color_title' => '#fff',
            'title' => 'Recomendamos pra você',
            'quantity' => 4,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_enabled_array
        ];
        $config16 = [
            'color_title' => '#fff',
            'title' => 'Top Trends (Os mais navegados)',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_home
        ];
        $config17 = [
            'color_title' => '#fff',
            'title' => 'Best Sellers (Os mais vendidos)',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_home
        ];
        $config18 = [
            'color_title' => '#fff',
            'title' => 'Outros clientes estão comprando',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_home
        ];
        $config19 = [
            'color_title' => '#fff',
            'title' => 'Remarketing on-site',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_enabled_array
        ];
        $config20 = [
            'color_title' => '#fff',
            'title' => 'Quem comprou, comprou também',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_product
        ];
        $config21 = [
            'color_title' => '#fff',
            'title' => 'Quem viu, viu também',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_product
        ];
        $config22 = [
            'color_title' => '#fff',
            'title' => 'Best Sellers da Categoria',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_enabled_array
        ];
        $config23 = [
            'color_title' => '#fff',
            'title' => 'Frequentemente comprando juntos',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_product
        ];
        $config24 = [
            'color_title' => '#fff',
            'title' => 'Lançamentos',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_home
        ];
        $config25 = [
            'color_title' => '#fff',
            'title' => 'Liquidação',
            'quantity' => 24,
            'intelligences' => [1],
            'container' => ['position' => 'bottom', "targetSelector" => '.roi-hero-container-1'],
            'responsive' => [
                ['name' => 'celular', "width" => 576, "items" => 1],
                ['name' => 'tablet', "width" => 768, "items" => 2],
                ['name' => 'desktop', "width" => 992, "items" => 4],
                ['name' => 'telas grandes', "width" => 1200, "items" => 4]
            ],
            'pages' => $pages_only_home
        ];

        $config_search = [
            'color_title'           => '#fff',
            'title'                 => 'Busca',
            'quantity'              => 24,
            'query_text'            => 'Encontramos {{PRODUCTS}} produtos em apenas {{TIME}} segundos.',
            'search_button_color'   => '#4ec244',
            'placeholder'           => 'Pesquise por um produto',
            'show_out_of_stock'     => true,
            'pages'                 => $pages_enabled_array,
        ];
        $config_autocomplete = [
            'color_title'   => '#fff',
            'title'         => 'Auto Complete',
            'quantity'      => 5,
            'pages'         => $pages_enabled_array,
        ];
        $config_dropdown = [
            'color_title'   => '#fff',
            'fixed_links'   => [
                [ 'title' => 'Iphone 11',                'link' => 'https://www.google.com/search?q=iphone+11'],
                [ 'title' => 'Galaxy S10',               'link' => 'https://www.google.com/search?q=galaxy+s10'],
                [ 'title' => 'Carregador Turbo Samsung', 'link' => 'https://www.google.com/search?q=carregador+turbo+samsung'],
                [ 'title' => 'Air Pods',                 'link' => 'https://www.google.com/search?q=air+pods'],
                [ 'title' => 'Rastrear meu pedido',      'link' => 'https://www.google.com/'],
            ],
            'dropdown_type' => 'term',
            'quantity'      => 5,
            'intelligences' => [1],
            'pages'         => $pages_enabled_array,
        ];

        $config_search_remarketing = [
            'color_title'   => '#fff',
            'title'         => 'Porque você buscou, recomendamos',
            'quantity'      => 4,
            'intelligences' => [1],
            'pages'         => $pages_enabled_array,
        ];

        \DB::table('feature_config_presets')->insert([
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::POPUP_RETENTION,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::POPUP_RETENTION,
                'config'            => json_encode($config1, true),
                'feature_theme_id' => '1',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::POPUP_SIDESTORE,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::POPUP_SIDESTORE,
                'config'            => json_encode($config2, true),
                'feature_theme_id' => '1',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::POPUP_CATEMAIL,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::POPUP_CATEMAIL,
                'config'            => json_encode($config3, true),
                'feature_theme_id' => '1',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::POPUP_PRODUCTOFTHEDAY,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::POPUP_PRODUCTOFTHEDAY,
                'config'            => json_encode($config4, true),
                'feature_theme_id' => '1',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::NOTIFICATION_FEATURE,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::NOTIFICATION_FEATURE,
                'config'            => json_encode($config5, true),
                'feature_theme_id' => '2',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::EMAIL_REMARKETING,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::EMAIL_REMARKETING,
                'config'            => json_encode($config6, true),
                'feature_theme_id' => '3',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::EMAIL_FIDELIZATION,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::EMAIL_FIDELIZATION,
                'config'            => json_encode($config7, true),
                'feature_theme_id' => '3',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::EMAIL_BIRTH,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::EMAIL_BIRTH,
                'config'            => json_encode($config8, true),
                'feature_theme_id' => '3',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::EMAIL_DISCOUNT,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::EMAIL_DISCOUNT,
                'config'            => json_encode($config9, true),
                'feature_theme_id'  => '3',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::EMAIL_PROMOTION,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::EMAIL_PROMOTION,
                'config'            => json_encode($config10, true),
                'feature_theme_id'  => '3',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::RECOVERY_CART,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::RECOVERY_CART,
                'config'            => json_encode($config11, true),
                'feature_theme_id'  => '4',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::RECOVERY_BILLET,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::RECOVERY_BILLET,
                'config'            => json_encode($config12, true),
                'feature_theme_id'  => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_1,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_1,
                'config'            => json_encode($config13, true),
                'feature_theme_id'  => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_2,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_2,
                'config'            => json_encode($config14, true),
                'feature_theme_id'  => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_3,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_3,
                'config'            => json_encode($config15, true),
                'feature_theme_id'  => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_4,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_4,
                'config'            => json_encode($config16, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],[
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_5,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_5,
                'config'            => json_encode($config17, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_6,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_6,
                'config'            => json_encode($config18, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_7,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_7,
                'config'            => json_encode($config19, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_8,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_8,
                'config'            => json_encode($config20, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_9,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_9,
                'config'            => json_encode($config21, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_10,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_10,
                'config'            => json_encode($config22, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_11,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_11,
                'config'            => json_encode($config23, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_12,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_12,
                'config'            => json_encode($config24, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SHELF_13,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SHELF_13,
                'config'            => json_encode($config25, true),
                'feature_theme_id' => '5',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SEARCH,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SEARCH,
                'config'            => json_encode($config_search, true),
                'feature_theme_id' => '6',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::AUTOCOMPLETE,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::AUTOCOMPLETE,
                'config'            => json_encode($config_autocomplete, true),
                'feature_theme_id' => '6',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::DROPDOWN,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::DROPDOWN,
                'config'            => json_encode($config_dropdown, true),
                'feature_theme_id' => '6',
                'status'            => true,
            ],
            [
                'id'                => hash('md2', microtime()),
                'name'              => 'Preset Feature ' . \RKT\Domains\Enums\FeatureIds::SEARCH_REMARKETING,
                'config_preset_id'  => '1',
                'feature_id'        => \RKT\Domains\Enums\FeatureIds::SEARCH_REMARKETING,
                'config'            => json_encode($config_search_remarketing, true),
                'feature_theme_id'  => '6',
                'status'            => true,
            ],
        ]);
    }
}
