<?php

use Illuminate\Database\Seeder;

class PromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('promotions')->insert([
            ['id' => 1, 'description' => 'Trial', 'status' => true, 'duration' => 7, 'event_id' => \RKT\Domains\Enums\Event::SIGNUP],
        ]);
    }
}
