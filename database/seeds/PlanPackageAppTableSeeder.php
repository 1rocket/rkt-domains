<?php

use Illuminate\Database\Seeder;

class PlanPackageAppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plan_package_apps')->insert([
            ['id' => hash('md2', microtime()), 'plan_id' => 1, 'package_app_id' => 1],
            ['id' => hash('md2', microtime()), 'plan_id' => 1, 'package_app_id' => 2],
            ['id' => hash('md2', microtime()), 'plan_id' => 1, 'package_app_id' => 3],
            ['id' => hash('md2', microtime()), 'plan_id' => 1, 'package_app_id' => 4],
            ['id' => hash('md2', microtime()), 'plan_id' => 1, 'package_app_id' => 5],
            ['id' => hash('md2', microtime()), 'plan_id' => 1, 'package_app_id' => 6],
            ['id' => hash('md2', microtime()), 'plan_id' => 2, 'package_app_id' => 1],
            ['id' => hash('md2', microtime()), 'plan_id' => 2, 'package_app_id' => 2],
            ['id' => hash('md2', microtime()), 'plan_id' => 2, 'package_app_id' => 3],
            ['id' => hash('md2', microtime()), 'plan_id' => 2, 'package_app_id' => 4],
            ['id' => hash('md2', microtime()), 'plan_id' => 2, 'package_app_id' => 5],
            ['id' => hash('md2', microtime()), 'plan_id' => 2, 'package_app_id' => 6],
            ['id' => hash('md2', microtime()), 'plan_id' => 3, 'package_app_id' => 1],
            ['id' => hash('md2', microtime()), 'plan_id' => 3, 'package_app_id' => 2],
            ['id' => hash('md2', microtime()), 'plan_id' => 3, 'package_app_id' => 3],
            ['id' => hash('md2', microtime()), 'plan_id' => 3, 'package_app_id' => 4],
            ['id' => hash('md2', microtime()), 'plan_id' => 3, 'package_app_id' => 5],
            ['id' => hash('md2', microtime()), 'plan_id' => 3, 'package_app_id' => 6],
        ]);
    }
}
