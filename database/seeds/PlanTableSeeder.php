<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plans')->insert([
            ['id' => 1, 'name' => 'Free', 'real' => 0.00, 'euro' => 0.00, 'dollar' => 0.00, 'status' => true, 'duration' => 1, 'type' => '2'],
            ['id' => 2, 'name' => 'Premium Mensal', 'real' => 0.00, 'euro' => 0.00, 'dollar' => 0.00, 'status' => true, 'duration' => 1, 'type' => '2'],
            ['id' => 3, 'name' => 'Premium Anual', 'real' => 0.00, 'euro' => 0.00, 'dollar' => 0.00, 'status' => true, 'duration' => 12, 'type' => '2'],
            ['id' => 4, 'name' => 'Disable', 'real' => 0.00, 'euro' => 0.00, 'dollar' => 0.00, 'status' => true, 'duration' => 0, 'type' => '2'],
        ]);
    }
}
