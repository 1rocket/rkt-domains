<?php

use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('events')->insert([
            ['id' => \RKT\Domains\Enums\Event::SIGNUP, 'name' => 'Cadastro', 'status' => true, 'type' => \RKT\Domains\Enums\EventType::ADM],
        ]);
    }
}
