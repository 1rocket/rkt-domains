<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            ['id' => \RKT\Domains\Enums\Roles::CLIENT, 'description' => 'Client'],
            ['id' => \RKT\Domains\Enums\Roles::ADMIN, 'description' => 'Admin'],
        ]);
    }
}
