<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUnlimited extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_apps', function (Blueprint $table) {
            $table->boolean('unlimited')->default(false);
        });

        Schema::table('promotion_package_apps', function (Blueprint $table) {
            $table->boolean('unlimited')->default(false);
        });

        Schema::table('client_package_apps', function (Blueprint $table) {
            $table->boolean('unlimited')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
