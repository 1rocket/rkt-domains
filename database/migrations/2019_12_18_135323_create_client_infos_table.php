<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_infos', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->timestamps();
            $table->softDeletes();

            $table->string('client_id')         ->unique();
            $table->string('facebook_id')                   ->nullable();
            $table->string('name')                          ->nullable();
            $table->string('type_update_product')           ->nullable();
            $table->string('xml_structure_id')              ->nullable();
            $table->string('url_xml')                       ->nullable();
            $table->string('email')             ->unique();
            $table->string('domain')            ->unique()  ->nullable();
            $table->string('platform_id')                   ->nullable();
            $table->string('segment_id')                    ->nullable();
            $table->string('street')                        ->nullable();
            $table->string('state')                         ->nullable();
            $table->string('district')                      ->nullable();
            $table->string('address_number')                ->nullable();
            $table->string('city')                          ->nullable();
            $table->string('zip_code')                      ->nullable();
            $table->string('complement')                    ->nullable();
            $table->string('cnpj')              ->unique()  ->nullable();
            $table->string('razao_social')                  ->nullable();
            $table->string('fantasy_name')                  ->nullable();
            $table->string('phone_number')                  ->nullable();
            $table->string('cell_number')                   ->nullable();
            $table->string('other_email')                   ->nullable();
            $table->string('other_contact')                 ->nullable();
            $table->string('preferred_contact')             ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_infos');
    }
}
