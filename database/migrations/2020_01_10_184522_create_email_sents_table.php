<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailSentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_sents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('client_id');
            $table->string('widget_id');
            $table->string('intelligence_id');
            $table->string('email');
            $table->string('sender');
            $table->string('content');
            $table->string('subject');
            $table->string('open_count');
            $table->string('click_count');
            $table->dateTime('send_date');
            $table->dateTime('view_date');
            $table->dateTime('click_date');
            $table->boolean('status');
            $table->boolean('isTest');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_sents');
    }
}
