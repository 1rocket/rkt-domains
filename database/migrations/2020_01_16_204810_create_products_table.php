<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->timestamps();
            $table->softDeletes();

            $table->string('client_id');
            $table->string('p_id');
            $table->string('sku')                   ->nullable();
            $table->string('title')                 ->nullable();
            $table->string('type')                  ->nullable();
            $table->text('description')             ->nullable();
            $table->decimal('price', 16, 2)         ->nullable();
            $table->decimal('sale_price', 16, 2)    ->nullable();
            $table->integer('discount')             ->nullable();
            $table->string('reference_id')          ->nullable();
            $table->string('department')            ->nullable();
            $table->string('category')              ->nullable();
            $table->string('subcategory')           ->nullable();
            $table->string('tags')                  ->nullable();
            $table->string('brand')                 ->nullable();
            $table->string('mpn')                   ->nullable();
            $table->string('gtin')                  ->nullable();
            $table->string('link', 500)             ->nullable();
            $table->string('image_link', 500)       ->nullable();
            $table->string('image_link_2', 500)     ->nullable();
            $table->integer('months')               ->nullable();
            $table->decimal('amount', 16, 2)        ->nullable();
            $table->integer('availability')         ->nullable();
            $table->string('update_time')           ->nullable();
            $table->json('variation')               ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
