<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeToNullableColumnsOfProductMetrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_metrics', function (Blueprint $table) {
            $table->integer('price')->nullable()->default(0)->change();
            $table->integer('views')->nullable()->default(0)->change();
            $table->integer('carts')->nullable()->default(0)->change();
            $table->integer('purchases')->nullable()->default(0)->change();
            $table->integer('searches')->nullable()->default(0)->change();
            $table->integer('loveds')->nullable()->default(0)->change();
            $table->integer('dislikes')->nullable()->default(0)->change();
            $table->integer('trashs')->nullable()->default(0)->change();
            $table->integer('copies')->nullable()->default(0)->change();
            $table->string('department')->nullable()->change();
            $table->string('p_id')->nullable()->change();
            $table->string('sku')->nullable()->change();
            $table->string('brand')->nullable()->change();
            $table->string('client_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nullable_columns_of_product_metrics', function (Blueprint $table) {
            //
        });
    }
}
