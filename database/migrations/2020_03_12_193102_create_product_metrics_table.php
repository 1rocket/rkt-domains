<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_metrics', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->timestamps();
            $table->softDeletes();

            $table->string('product_id');
            $table->integer('price');
            $table->integer('views');
            $table->integer('carts');
            $table->integer('purchases');
            $table->integer('searches');
            $table->integer('loveds');
            $table->integer('dislikes');
            $table->integer('trashs');
            $table->integer('copies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_metrics');
    }
}
