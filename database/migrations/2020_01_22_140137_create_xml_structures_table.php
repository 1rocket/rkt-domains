<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXmlStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xml_structures', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->timestamps();
            $table->softDeletes();

            $table->string('type_structure')        ->nullable();
            $table->string('item')                  ->nullable();
            $table->string('p_id')                  ->nullable();
            $table->string('sku')                   ->nullable();
            $table->string('title')                 ->nullable();
            $table->string('type')                  ->nullable();
            $table->string('description')           ->nullable();
            $table->string('price')                 ->nullable();
            $table->string('sale_price')            ->nullable();
            $table->string('discount')              ->nullable();
            $table->string('reference_id')          ->nullable();
            $table->string('department')            ->nullable();
            $table->string('category')              ->nullable();
            $table->string('subcategory')           ->nullable();
            $table->string('tags')                  ->nullable();
            $table->string('brand')                 ->nullable();
            $table->string('mpn')                   ->nullable();
            $table->string('gtin')                  ->nullable();
            $table->string('link')                  ->nullable();
            $table->string('image_link')            ->nullable();
            $table->string('image_link_2')          ->nullable();
            $table->string('months')                ->nullable();
            $table->string('amount')                ->nullable();
            $table->string('variation')             ->nullable();
            $table->string('availability')          ->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xml_structures');
    }
}
