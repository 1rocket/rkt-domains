<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionPackageAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_package_apps', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->timestamps();
            $table->softDeletes();

            $table->string('promotion_id');
            $table->string('package_app_id');
            $table->integer('period')->default(\RKT\Domains\Enums\Period::UNLIMITED);
            $table->bigInteger('quantity');
            $table->float('real');
            $table->float('dollar');
            $table->float('euro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_package_apps');
    }
}
