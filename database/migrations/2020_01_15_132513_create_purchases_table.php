<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {

            $table->string('id')->primary();
            $table->timestamps();
            $table->softDeletes();

            $table->string('session_id');
            $table->string('url');
            $table->string('platform_purchase_id');
            $table->integer('total_price');
            $table->string('currency');
            $table->integer('items_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
